#include <iostream>
#include <jsonrpc/rpc.h>
#include <jsonrpc/connectors/httpserver.h>

#include "abstractmystubserver.h"

using namespace jsonrpc;
using namespace std;

class MyStubServer : public AbstractMyStubServer
{
    public:
        MyStubServer();
        virtual void notifyServer();
        virtual std::string sayHello(const std::string& name);
};

MyStubServer::MyStubServer() :
    AbstractMyStubServer(new HttpServer(8080))
{
    std::cout << "Constructor to MyStubServer called" << std::endl;
}

void MyStubServer::notifyServer()
{
    std::cout << "Server got notified" << std::endl;
}

string MyStubServer::sayHello(const string &name)
{
    std::cout << "Got request to /sayHello" << std::endl;
    return "Hello " + name;
}

int main()
{
    MyStubServer s;
    s.StartListening();
    getchar();
    s.StopListening();
    return 0;
}
