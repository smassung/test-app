$ ->
  console.log "Loaded page!"
  $.ajax "http://localhost:9001/json-api",
    type: "POST"
    dataType: "json"
    data: JSON.stringify
      jsonrpc: 2.0
      method: "sayHello"
      params:
        name: "world"
      id: 1
    success: (data, stat, xhr) -> console.log data.result
    failure: (axhr, stat, err) -> console.log "Failed!"
