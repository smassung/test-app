# Project Setup

- Download the dependency `libjson-rpc-cpp` and install it.

- Set up the server project

`cd cpp/build`

`cmake ..`

# Running the Web Page

- Compile the coffeescript files

`cd web`

`make`

- Start the web server

`python2 -m SimpleHTTPServer`

- Compile the C++ server

`cd cpp/build`

`make`

- Start the C++ server

`cd cpp/build`

`./api-server`

- Install the node packages

`cd web/node`

`npm install`

- Start the proxy server

`node proxy.js`

- View the page at `http://localhost:9001`
